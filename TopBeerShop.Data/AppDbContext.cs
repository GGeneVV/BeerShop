﻿
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using TopBeerShop.Data.Configurations;
using TopBeerShop.Models;

namespace TopBeerShop.Data
{
    public class AppDbContext : IdentityDbContext<User, Role, int>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }      
        public DbSet<Country> Countries { get; set; }

        public DbSet<Review> Reviews { get; set; }
        public DbSet<BeerStyle> BeerStyles { get; set; }
        public DbSet<WishList> WishLists { get; set; }

       

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new BeerConfig());
            builder.ApplyConfiguration(new BreweryConfig());
            builder.ApplyConfiguration(new UserConfig());
            builder.ApplyConfiguration(new ReviewConfig());
            builder.ApplyConfiguration(new WishListConfig());
            builder.ApplyConfiguration(new BeerStylesConfig());
            builder.ApplyConfiguration(new WishListConfig());
        }


    }
}
