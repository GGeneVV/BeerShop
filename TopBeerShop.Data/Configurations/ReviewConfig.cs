﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Data.Configurations
{
    public class ReviewConfig : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder
                .HasOne(review => review.Beer)
                .WithMany(beer => beer.Reviews)
                .HasForeignKey(r => r.BeerId);

            builder
                .HasOne(u => u.User)
                .WithMany(r => r.UserReviews)
                .HasForeignKey(u => u.UserId);

        }
    }

}