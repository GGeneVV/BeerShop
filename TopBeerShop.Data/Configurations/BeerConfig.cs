﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Data.Configurations
{
    public class BeerConfig :IEntityTypeConfiguration<Beer>
    {
      public void Configure(EntityTypeBuilder<Beer> builder)
        {
           
            builder
                .HasOne(b => b.BeerStyles)
                .WithMany(s => s.Beers)
                .HasForeignKey(b => b.BeerStyleId);

            builder
                .HasOne(b => b.Brewery)
                .WithMany(b => b.Beers)
                .HasForeignKey(br => br.BreweryId);
        }

    }
    
}
