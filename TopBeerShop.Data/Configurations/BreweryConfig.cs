﻿using Microsoft.EntityFrameworkCore;

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Data.Configurations
{
    public class BreweryConfig : IEntityTypeConfiguration<Brewery>
    {
        public void Configure(EntityTypeBuilder<Brewery> builder)
        {
            builder
                .HasOne(brewery => brewery.Country)
                .WithMany(country => country.Breweries)
                .HasForeignKey(brewery => brewery.CountryId);
        }
    }
}
