﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Data.Configurations
{
    public class WishListConfig: IEntityTypeConfiguration<WishList>
    {
        public void Configure(EntityTypeBuilder<WishList> builder)
        {
            builder.HasKey(w => new {w.BeersId, w.UserId });
            builder
                .HasOne(wishlist => wishlist.Beers)
                .WithMany(beer => beer.WishLists)
                .HasForeignKey(wishlist=>wishlist.BeersId);

            builder.HasOne(w => w.Users)
                .WithMany(u => u.WishLists)
                .HasForeignKey(u=>u.UserId);         
          }
    }
}
