﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TopBeerShop.Data;

namespace TopBeerShop.Services.TESTS
{

    public class Utils
    {
        public static DbContextOptions<AppDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}