﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;

using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Services.TESTS.BreweryServiceTests
{
    [TestClass]
    public class GetBreweryIdByName_Should
    {

        [TestMethod]
        public void ReturnCorrectBreweryId_When_IsValid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ReturnCorrectBreweryId_When_IsValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("25/10/2020")
            };

            var brewery = new Brewery
            {
                BreweryId = 1,
                Name = "TopBrewery",
                Country = country,
                CreatedOn = DateTime.Parse("25/10/2020")
            };

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.SaveChanges();
            }

            //Act&Assert


            var mapper = new Mock<IMapper>();
            mapper.Setup(x => x.Map<BreweryDTO>(It.IsAny<Brewery>()))
                .Returns(new BreweryDTO
                {
                    BreweryId = 1,
                    Name = "TopBrewery",
                    Website = null,
                    Address = null,
                    Beers = null,
                    CountryId = 1,
                    CountryName = "Bulgaria"
                });

            using (var actContext = new AppDbContext(options))
            {
                var sut = new BreweryService(actContext, mapper.Object);
                var result = sut.GetId("TopBrewery");

                Assert.IsTrue(result == 1);
            }
        }


        [TestMethod]
        public void ReturnBreweryId_When_ParamNotValid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ReturnBreweryId_When_ParamNotValid));

            //Act&Assert
            var mapper = new Mock<IMapper>();
        
            using (var actContext = new AppDbContext(options))
            {
                var sut = new BreweryService(actContext, mapper.Object);
                Assert.ThrowsException<System.NullReferenceException>(() => sut.GetId("Not Valid"));
            }
        }
    }
}
