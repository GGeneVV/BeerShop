﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;
using TopBeerShop.Services.Providers;
using TopBeerShop.Services.Providers.Contracts;
using TopBeerShop.Web.Services;

namespace TopBeerShop.Services.TESTS.BreweryServiceTests
{
    [TestClass]
    public class EditBrewery_Should
    {
        [TestMethod]
        public void EditValidBrewery()
        {
            //Arange
            var options = Utils.GetOptions(nameof(EditValidBrewery));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("25/10/2020")
            };

            var brewery = new Brewery
            {
                BreweryId = 1,
                Name = "TopBrewery",
                Country = country,
                CreatedOn = DateTime.Parse("25/10/2020")
            };

            string newBreweryName = "Edited Name";

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Countries.Add(country);

                arrangeContext.SaveChanges();
            }

            brewery.Name = newBreweryName;

            var mapper = new Mock<IMapper>();
            mapper.Setup(x => x.Map<BreweryDTO>(It.IsAny<Brewery>()))
                .Returns(new BreweryDTO
                {
                    BreweryId = 1,
                    Name = "Edited Name",
                    Website = null,
                    Address = null,
                    Beers = null,
                    CountryId = 1,
                    CountryName = "Bulgaria"
                });

            using (var actContext = new AppDbContext(options))
            {
                var sut = new BreweryService(actContext, mapper.Object);
                var result = sut.UpdateBrewery(new BreweryDTO
                {
                    BreweryId = 1,
                    Name = "Edited Name",
                    Website = null,
                    Address = null,
                    Beers = null,
                    CountryId = 1,
                    CountryName = "Bulgaria"
                });

                Assert.IsTrue(result.CompareBreweryDTO(brewery));
            }
        }

        [TestMethod]
        public void ThrowErrorWhen_Invalid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ThrowErrorWhen_Invalid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("25/10/2020")
            };

            var brewery = new Brewery
            {
                BreweryId = 1,
                Name = "TopBrewery",
                Country = country,
                CreatedOn = DateTime.Parse("25/10/2020")
            };

            string newBreweryName = "Edited Name";

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Countries.Add(country);

                arrangeContext.SaveChanges();
            }

            brewery.Name = newBreweryName;

            var mapper = new Mock<IMapper>();
          
            using (var actContext = new AppDbContext(options))
            {
                var sut = new BreweryService(actContext, mapper.Object);
                                              

                Assert.ThrowsException<Exception>(() => sut.UpdateBrewery(new BreweryDTO
                {
                    BreweryId = 4,
                    Name = "Edited Name",
                    Website = null,
                    Address = null,
                    Beers = null,
                    CountryId = 1,
                    CountryName = "Bulgaria"
                }));
            }
        }
    }

}
