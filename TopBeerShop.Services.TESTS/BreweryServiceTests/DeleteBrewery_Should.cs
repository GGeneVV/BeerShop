﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Services.TESTS.BreweryServiceTests
{
    [TestClass]
    public class DeleteBrewery_Should
    {
        [TestMethod]
        public void ReturnTrueWhen_Valid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrueWhen_Valid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("24/8/2020")
            };

            var brewery = new Brewery
            {
                BreweryId = 1,
                Name = "TopBrewery",
                Country = country,
                CreatedOn = DateTime.Parse("25/9/2020")
            };

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.SaveChanges();
            }

            var mapper = new Mock<IMapper>();

            mapper.Setup(x => x.Map<BreweryDTO>(It.IsAny<Brewery>()))
                .Returns(new BreweryDTO
                {
                    BreweryId = 1,
                    Name = "TopBrewery",
                    Website = null,
                    Address = null,
                    Beers = null,
                    CountryId = 1,
                    CountryName = "Bulgaria",
                });


            using (var assertContext = new AppDbContext(options))
            {                
                var sut = new BreweryService(assertContext, mapper.Object);
                //Act
                var result = sut.DeleteBrewery(1);
                //Assert
                Assert.IsTrue(result);
            }
        }


        [TestMethod]
        public void ReturnFalseWhen_Invalid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalseWhen_Invalid));         
            var mapper = new Mock<IMapper>();

            using (var assertContext = new AppDbContext(options))
            {
                var sut = new BreweryService(assertContext, mapper.Object);

                //Act
                var result = sut.DeleteBrewery(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

    }
}
