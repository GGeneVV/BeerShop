﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Services.TESTS
{
    [TestClass]
    public class GetAllBreweries_Should
    {
        [TestMethod]
        public void ReturnCorrectBreweriesDTO_When_IsValid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ReturnCorrectBreweriesDTO_When_IsValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("24/8/2020")
            };
            var secCountry = new Country
            {
                Id = 2,
                Name = "Germany",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("24/8/2020")
            };
            var thirdCountry = new Country
            {
                Id = 3,
                Name = "Spain",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("24/8/2020")
            };

            var brewery = new Brewery
            {
                BreweryId = 1,
                Name = "TopBrewery",
                Country = country,
                CreatedOn = DateTime.Parse("25/9/2020")
            };

            var secBrewery = new Brewery
            {
                BreweryId = 2,
                Name = "secBrewery",
                Country = country,
                CreatedOn = DateTime.Parse("26/10/2020")
            };

            var thirdBrewery = new Brewery
            {
                BreweryId = 3,
                Name = "thirdBrewery",
                Country = country,
                CreatedOn = DateTime.Parse("27/10/2020")
            };

            var breweryTestList = new List<Brewery>();
            breweryTestList.Add(brewery);
            breweryTestList.Add(secBrewery);
            breweryTestList.Add(thirdBrewery);

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Breweries.Add(secBrewery);
                arrangeContext.Breweries.Add(thirdBrewery);
                arrangeContext.SaveChanges();
            }

            //Act&Assert        
            var mapper = new Mock<IMapper>();
            //mapper.Setup(x => x.Map<BreweryDTO>(It.IsAny<Brewery>()))
            //    .Returns(new BreweryDTO
            //    {
            //        BreweryId = 1,
            //        Name = "TopBrewery",
            //        Website = null,
            //        Address = null,
            //        Beers = null,
            //        CountryId = 1,
            //        CountryName = "Bulgaria"
            //    });

            using (var actContext = new AppDbContext(options))
            {
                var sut = new BreweryService(actContext, mapper.Object);
        
                var result = sut.GetAllBreweries().ToList();

                //Assert
                for (int i = 0; i < result.Count; i++)
                {
                    Assert.IsTrue(result[i].CompareBreweryDTO(breweryTestList[i]));
                }              
            }
        }

        [TestMethod]
        public void ReturnNullhen_Empty()
        {
            var options = Utils.GetOptions(nameof(ReturnNullhen_Empty));
            var mapper = new Mock<IMapper>();

            using (var assertContext = new AppDbContext(options))
            {
                var sut = new BreweryService(assertContext, mapper.Object);

                var result = sut.GetAllBreweries();

                Assert.IsTrue(result.Count == 0);
            }

        }
    }
}

