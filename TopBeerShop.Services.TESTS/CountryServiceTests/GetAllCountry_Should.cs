﻿using AutoMapper;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;


namespace TopBeerShop.Services.TESTS.BreweryServiceTests
{
    [TestClass]
    public class GetAllCountry_Should
    {
        [TestMethod]
        public void ReturnCorrectCountriesDTO_When_IsValid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ReturnCorrectCountriesDTO_When_IsValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("24/8/2020")
            };
            var secCountry = new Country
            {
                Id = 2,
                Name = "Germany",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("24/8/2020")
            };
            var thirdCountry = new Country
            {
                Id = 3,
                Name = "Spain",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("24/8/2020")
            };

            var countryTestList = new List<Country>();
            countryTestList.Add(country);
            countryTestList.Add(secCountry);
            countryTestList.Add(thirdCountry);

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Countries.Add(secCountry);
                arrangeContext.Countries.Add(thirdCountry);
                arrangeContext.SaveChanges();
            }

            //Act&Assert

            var mapper = new Mock<IMapper>();
            
            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                var result = sut.GetAllCountries().ToList();
                               
                for (int i = 0; i < result.Count; i++)
                {                    
                    Assert.IsTrue(result[i].CompareCountryDTO(countryTestList[i]));
                }
            }
        }

        [TestMethod]
        public void ReturnFalseWhen_Invalid()
        {
            var options = Utils.GetOptions(nameof(ReturnFalseWhen_Invalid));

            //Act&Assert

            var mapper = new Mock<IMapper>();

            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                {
                    Assert.ThrowsException<ArgumentNullException>(() => sut.GetCountry(1));
                }
            }
        }
    }

}
