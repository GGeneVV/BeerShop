﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;
using TopBeerShop.Services.Providers;
using TopBeerShop.Services.Providers.Contracts;
using TopBeerShop.Web.Services;

namespace TopBeerShop.Services.TESTS.BreweryServiceTests
{
    [TestClass]
    public class GetCountry_Should
    {
        [TestMethod]
        public void ReturnCorrectCountryDTO_When_IsValid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ReturnCorrectCountryDTO_When_IsValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("25/10/2020")
            };          

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            //Act&Assert

            var mapper = new Mock<IMapper>();
            
            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                var result = sut.GetCountry(1);

                Assert.IsTrue(result.CompareCountryDTO(country));
            }
        }

        [TestMethod]
        public void ReturnFalseWhen_Invalid()
        {
            var options = Utils.GetOptions(nameof(ReturnFalseWhen_Invalid));

            //Act&Assert

            var mapper = new Mock<IMapper>();

            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                {
                    Assert.ThrowsException<ArgumentNullException>(() => sut.GetCountry(1));
                }
            }
        }
    }

}
