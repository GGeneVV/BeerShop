﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;


namespace TopBeerShop.Services.TESTS.BreweryServiceTests
{
    [TestClass]
    public class DeleteCountry_Should
    {
        [TestMethod]
        public void ReturnTrue_When_IsValid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_IsValid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("25/10/2020")
            };          

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            //Act&Assert

            var mapper = new Mock<IMapper>();
            
            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                var result = sut.DeleteCountry(1);
               
                Assert.IsTrue(result);
            }
        }

        [TestMethod]
        public void ReturnFalseWhen_Invalid()
        {
            var options = Utils.GetOptions(nameof(ReturnFalseWhen_Invalid));

            //Act&Assert

            var mapper = new Mock<IMapper>();

            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                var result = sut.DeleteCountry(1);
                                
                Assert.IsFalse(result);
            }
        }
    }

}
