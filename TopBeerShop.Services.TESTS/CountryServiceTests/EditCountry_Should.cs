﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.CompareModels;

using TopBeerShop.Services.DTOs;


namespace TopBeerShop.Services.TESTS.BreweryServiceTests
{
    [TestClass]
    public class EditCountry_Should
    {
        [TestMethod]
        public void EditValidCountry()
        {
            //Arange
            var options = Utils.GetOptions(nameof(EditValidCountry));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("25/10/2020")
            };

            string newCountryName = "TelericCountry";

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            country.Name = newCountryName;

            var mapper = new Mock<IMapper>();           

            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                var result = sut.Edit(new CountryDTO
                {
                    Id = 1,
                    Name = "TelericCountry",                   
                });

                Assert.IsTrue(result.CompareCountryDTO(country));
            }
        }

        [TestMethod]
        public void ThrowErrorWhen_Invalid()
        {
            //Arange
            var options = Utils.GetOptions(nameof(ThrowErrorWhen_Invalid));

            var country = new Country
            {
                Id = 1,
                Name = "Bulgaria",
                Breweries = new List<Brewery>(),
                CreatedOn = DateTime.Parse("25/10/2020")
            };
                        
            string newConutryName = "TelerikCountry";

            using (var arrangeContext = new AppDbContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            country.Name = newConutryName;

            var mapper = new Mock<IMapper>();
          
            using (var actContext = new AppDbContext(options))
            {
                var sut = new CountryService(actContext, mapper.Object);
                                              

                Assert.ThrowsException<ArgumentNullException>(() => sut.Edit(new CountryDTO
                {
                    Id = 4,
                    Name = "Edited Name"                   
                }));
            }
        }
    }

}
