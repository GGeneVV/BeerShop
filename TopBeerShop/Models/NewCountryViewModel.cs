﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TopBeerShop.Web.Models
{
    public class NewCountryViewModel
    {
        public string OldName { get; set; }
        public string NewName { get; set; }
    }
}
