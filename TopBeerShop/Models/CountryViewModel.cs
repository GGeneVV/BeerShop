﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TopBeerShop.Web.Models
{
    public class CountryViewModel
    {
        public int Id { get; set; }
        [MinLength(3), MaxLength(50)]
        public string Name { get; set; }
       
    }
}
