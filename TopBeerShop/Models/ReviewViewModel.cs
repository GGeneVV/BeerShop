﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace TopBeerShop.Web.Models
{
    public class ReviewViewModel
    {
        public int ReviewId { get; set; }
        [Range(1,10)]
        public int Rating { get; set; }
        [MinLength(3), MaxLength(150)]
        public string Text { get; set; }
        public bool Liked { get; set; }
        public bool Disliked { get; set; }

        public bool IsDeleted { get; set; }

        public bool Flagged { get; set; }
        public int UserId { get; set; }
        public int BeerId { get; set; }
        public string UserName { get; set; }
        public string BeerName { get; set; }
        
    }
}
