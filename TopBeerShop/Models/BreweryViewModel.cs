﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TopBeerShop.Models;

namespace TopBeerShop.Web.Models
{
    public class BreweryViewModel
    {      
        public int BreweryId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }        
        public int CountryId { get; set; }
        public string CountryName { get; set; }      
    }
}
