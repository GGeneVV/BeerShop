﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TopBeerShop.Models;

namespace TopBeerShop.Web.Models
{
    public class BeerViewModel
    {

        public int BeerId { get; set; }
        [Required]
        [MinLength(3), MaxLength(20)]
        public string BeerName { get; set; }
        public double ABV { get; set; }
        public double Price { get; set; }

        public int BreweryId { get; set; }
        public int StyleId { get; set; }
        

        public DateTime CreatedOn { get; set; }


    }
}
