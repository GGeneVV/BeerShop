﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TopBeerShop.Web.Models
{
    public class BeerStyleViewModel
    {
        public int Id { get; set; }
        [MinLength(3), MaxLength(100)]
        public string Style { get; set; }
        public string Description { get; set; }
        
    }
}
