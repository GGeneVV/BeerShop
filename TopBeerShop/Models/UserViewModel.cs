﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TopBeerShop.Services.DTO;

namespace TopBeerShop.Web.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsBanned { get; set; }
        public string BanDescription { get; set; }
        public ICollection<WishListDTO> WishList { get; set; }     
    }
}
