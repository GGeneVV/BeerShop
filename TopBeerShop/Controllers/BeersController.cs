﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.Providers.Contracts;
using TopBeerShop.Web.Models;
using TopBeerShop.Web.Pagination;

namespace TopBeerShop.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService _beerService;
        private readonly IMapper _mapper;
        private readonly IBeerStyleService _beerStyleService;
        private readonly IBreweryService _breweryService;
        private readonly ICountryService _countryService;

        public BeersController(IBeerService beerService, IMapper mapper,
            IBeerStyleService beerStyleService, IBreweryService breweryService, ICountryService countryService)
        {
            _beerService = beerService;
            _mapper = mapper;
            _beerStyleService = beerStyleService;
            _breweryService = breweryService;
            _countryService = countryService;

        }

        // GET: Beers
        public  IActionResult Index(
            string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["ABVSortParm"] = sortOrder == "ABV" ? "abv_desc" : "ABV";
            ViewData["PriceSortParm"] = sortOrder == "Price" ? "price_desc" : "Price";


            if (searchString != null) {
                pageNumber = 1;
            }
            else {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;


            var beers = from b in _beerService.GetAllBeers().Select(x => _mapper.Map<BeerViewModel>(x))
                        select b;
            if (!String.IsNullOrEmpty(searchString)) {
                beers = beers.Where(s => s.BeerName.Contains(searchString));

            }
            switch (sortOrder) {
                case "name_desc":
                    beers = beers.OrderByDescending(b => b.BeerName);
                    break;
                case "ABV":
                    beers = beers.OrderBy(b => b.ABV);
                    break;
                case "abv_desc":
                    beers = beers.OrderByDescending(s => s.ABV);
                    break;
                case "Price":
                    beers = beers.OrderBy(b => b.Price);
                    break;
                case "price_desc":
                    beers = beers.OrderByDescending(s => s.Price);
                    break;
                default:
                    beers = beers.OrderBy(s => s.BeerName);
                    break;
            }
            
            int pageSize = 5;
            return View(PaginatedList<BeerViewModel>.Create(beers.AsQueryable(), pageNumber ?? 1, pageSize));
            
        }

        // GET: Beers/Details/5
        public IActionResult Details(int Id)
        {

            try {

                var beer = _beerService.GetBeer(Id);

                if (beer == null) {
                    return NotFound();
                }

                var beerVM = _mapper.Map<BeerViewModel>(beer);

                return View(beerVM);
            }
            catch (Exception) {

                throw new ArgumentException();
            }

        }

        // GET: Beers/Create
        // GET: Beer/Create
        [HttpGet]
        public IActionResult CreateBeer()
        {
            var breweriesDTO = _breweryService.GetAllBreweries();
            var breweriesVM = breweriesDTO.Select(x => _mapper.Map<BreweryViewModel>(x));

            //var countriesDTO = _countryService.GetAllCountries();
            //var countriesVM = countriesDTO.Select(x => _mapper.Map<CountryViewModel>(x));

            var styleDTO = _beerStyleService.GetAllBeerStyles();
            var styleVM = styleDTO.Select(x => _mapper.Map<BeerStyleViewModel>(x));

            ViewData["BreweryId"] = new SelectList(breweriesVM, "BreweryId", "Name");

            ViewData["StyleId"] = new SelectList(styleVM, "Id", "Style");

            return View();
        }

        [HttpPost]
        public IActionResult CreateBeer([Bind("Id,BeerName,ABV,Price,BreweryId,StyleId")] BeerViewModel beerViewModel)
        {
            var beerDTO = _mapper.Map<BeerDTO>(beerViewModel);

            _beerService.CreateBeer(beerDTO);


            return RedirectToAction("Index");
        }




        // GET: Beers/Edit/5
        [HttpGet]
        public IActionResult Edit(int id)
        {

            var breweriesDTO = _breweryService.GetAllBreweries();
            var breweriesVM = breweriesDTO.Select(x => _mapper.Map<BreweryViewModel>(x));
            var styleDTO = _beerStyleService.GetAllBeerStyles();
            var styleVM = styleDTO.Select(x => _mapper.Map<BeerStyleViewModel>(x));

            var beer = _beerService.GetBeer(id);

            var beerVM = _mapper.Map<BeerViewModel>(beer);

            ViewData["BreweryId"] = new SelectList(breweriesVM, "BreweryId", "Name");

            ViewData["StyleId"] = new SelectList(styleVM, "Id", "Style");

            return View(beerVM);


        }

        // POST: Beers/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit([Bind("Id,BeerName,ABV,Price,BreweryId,StyleId")] BeerViewModel model)
        {

            try {
                var beerDTO = _mapper.Map<BeerDTO>(model);

                _beerService.UpdateBeer(beerDTO);

                var beerVM = _mapper.Map<BeerViewModel>(beerDTO);


                //return RedirectToAction(nameof(Index));
                return RedirectToAction(nameof(Index));
            }
            catch (Exception) {

                throw;
            }
        }


        // GET: Beers/Delete/5
        public IActionResult Delete(int id)
        {
            var beerDTO = _beerService.GetBeer(id);

            var beerVM = _mapper.Map<BeerViewModel>(beerDTO);


            return View(beerVM);

        }

        // POST: Countries/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(BeerViewModel beer)
        {

            _beerService.DeleteBeer(beer.BeerId);

            return RedirectToAction(nameof(Index));
        }

        // POST: Beers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public IActionResult DeleteConfirmed(int id)
        //{
        //    var beer = _service.DeleteBeer(id);
        //    return RedirectToAction(nameof(Index));
        //}

        private bool BeerExists(int id)
        {
            try {
                var beer = _beerService.GetBeer(id);

                return true;
            }
            catch (Exception) {

                throw new ArgumentException();
            }
        }
    }
}
