﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Web.Models;

namespace TopBeerShop.Web.Controllers
{
    public class HomeController : Controller

    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBeerService _beerService;
        private readonly IBreweryService _breweryService;
        private readonly IMapper _mapper;
        public HomeController(ILogger<HomeController> logger, IBeerService beerService, IMapper mapper,
            IBreweryService breweryService)
        {
            _logger = logger;
            _beerService = beerService;
            _mapper = mapper;
            _breweryService = breweryService;
        }
        // GET: HomeController
        public ActionResult Index()
        {
            //logic to get top beers and top breweries
            //var model = new HomeIndexViewModel{
            //    TopRatedBeers = //this.service.GetTopBeers(),
            //    To
            //}
            var homeViewModel = new HomeIndexViewModel();

            var topRatedBeers = _beerService.GetAllBeers()
                .OrderByDescending(x => x.ABV)
                .Take(6)
                .Select(x => _mapper.Map<BeerViewModel>(x));
            homeViewModel.TopRatedBeers = topRatedBeers.ToList();

            var topRatedBreweries = _breweryService.GetAllBreweries()
                .OrderByDescending(br => br.Beers.Count)
                .Take(6)
                .Select(x => _mapper.Map<BreweryViewModel>(x));
            homeViewModel.TopRatedBreweries = topRatedBreweries.ToList();

            return View(homeViewModel);
        }

        //public IActionResult IndexBrewery()
        //{
        //    var breweries = _breweryService.GetAllBreweries();

        //    var breweryVM = breweries.Select(x => _mapper.Map<BreweryViewModel>(x));

        //    var breweryModelTopVM = new HomeIndexViewModel {
        //        TopRatedBreweries = breweryVM.Select(x => new BreweryViewModel {
        //            BreweryId = x.BreweryId,
        //            Name = x.Name,
        //            Website = x.Website,
        //            CountryName = x.CountryName,

        //        })
        //        .OrderByDescending(x => x.Name)
        //        .Take(6)
        //        .ToList()
        //    };

        //    return View(breweryModelTopVM);
        //}
        // return View(model);
        public IActionResult TopBeers()
        {
            var beers = _beerService.GetAllBeers();

            var beersView = _mapper.Map<BeerViewModel>(beers);

            return View(beersView);
        }


        public IActionResult TopBreweries()
        {
            var brewery = _breweryService.GetAllBreweries();

            var breweriesVM = _mapper.Map<BreweryViewModel>(brewery);

            return View(breweriesVM);
        }


        // GET: HomeController/Details/5
        public ActionResult Details(int id)
        {
            try {
                var beer = _beerService.GetBeer(id);
                var brewery = _breweryService.GetBrewery(id);

                
                var breweryView = _mapper.Map<BreweryViewModel>(brewery);
                var beerViewMOdel = _mapper.Map<BeerViewModel>(beer);

                if (breweryView != null) {
                    return View(breweryView);
                }
                else {

                    return View(beerViewMOdel);
                }


            }
            catch (Exception) {

                throw new ArgumentException();
            }
        }

        // GET: HomeController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try {
                return RedirectToAction(nameof(Index));
            }
            catch {
                return View();
            }
        }

        // GET: HomeController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HomeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try {
                return RedirectToAction(nameof(Index));
            }
            catch {
                return View();
            }
        }

        // GET: HomeController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HomeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try {
                return RedirectToAction(nameof(Index));
            }
            catch {
                return View();
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
