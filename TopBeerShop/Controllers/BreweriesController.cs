﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.Services;
using TopBeerShop.Web.Models;

namespace TopBeerShop.Web.Controllers
{
    public class BreweriesController : Controller
    {       
        private readonly IBreweryService _breweryService;
        private readonly IMapper _mapper;
        private readonly ICountryService _countryService;

        public BreweriesController(IBreweryService breweryService, IMapper mapper, ICountryService countryService)
        {           
            _breweryService = breweryService;
            _mapper = mapper;
            _countryService = countryService;
        }

        // GET: Breweries
        public IActionResult Index()
        {
            var breweries = _breweryService.GetAllBreweries().Select(x => _mapper.Map<BreweryViewModel>(x));
            return View(breweries);
        }

        // GET: Breweries/Details/5
        public IActionResult Details(int id)
        {
            if (id == null) {
                return NotFound();
            }

            var brewery = _breweryService.GetBrewery(id);             
                
            if (brewery == null) {
                return NotFound();
            }
            var breweryVM = _mapper.Map<BreweryViewModel>(brewery);
            return View(breweryVM);
        }

        // GET: Breweries/Create
        [Authorize]
        public IActionResult Create()
        {
            ViewData["CountryName"] = new SelectList(_countryService.GetAllCountries(), "Name", "Name");
            return View();
        }

        // POST: Breweries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("BreweryId,Name,Website,Address,CountryName,Description")] BreweryViewModel brewery)
        {
            var breweryDTO = _mapper.Map<BreweryDTO>(brewery);

            if (ModelState.IsValid) {
                _breweryService.CreateBrewery(breweryDTO);

                return RedirectToAction(nameof(Index));
            }

            ViewData["Country Name"] = new SelectList(_countryService.GetAllCountries(), "Name", "Name", brewery.CountryName);
            return View(brewery);
        }

        // GET: Breweries/Edit/5
        [Authorize]
        public IActionResult Edit(int id)
        {           
            var breweryDTO = _breweryService.GetBrewery(id);

            if (breweryDTO == null) {
                return NotFound();
            }
            var countriesDTO = _countryService.GetAllCountries();
            var countriesVM = countriesDTO.Select(x => _mapper.Map<CountryViewModel>(x));
            ViewData["CountryName"] = new SelectList(countriesVM, "Name", "Name", breweryDTO.CountryName);

            var breweryVM = _mapper.Map<BreweryViewModel>(breweryDTO);

            return View(breweryVM);
        }

        // POST: Breweries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(BreweryViewModel breweryVM)
        {
            try {
                var breweryDTO = _mapper.Map<BreweryDTO>(breweryVM);
                _breweryService.UpdateBrewery(breweryDTO);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception) {

                throw;
            }

        }

        // GET: Breweries/Delete/5
        [Authorize]
        public IActionResult Delete(int id)
        {
            var brewery = _breweryService.GetBrewery(id);

            var breweryVM = _mapper.Map<BreweryViewModel>(brewery);


            return View(breweryVM);
        }

        // POST: Breweries/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            _breweryService.DeleteBrewery(id);
            return RedirectToAction(nameof(Index));
        }

        //private bool BreweryExists(int id)
        //{
        //    return _breweryService.Get(e => e.BreweryId == id);
        //}
    }
}
