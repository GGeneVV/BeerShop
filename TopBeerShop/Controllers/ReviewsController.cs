﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Internal.Account.Manage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SQLitePCL;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTO;
using TopBeerShop.Services.Providers.Contracts;
using TopBeerShop.Web.Models;

namespace TopBeerShop.Web.Controllers
{
    public class ReviewsController : Controller
    {
        private readonly IReviewService _reviewService;
        private readonly IMapper _mapper;
        private readonly IBeerService _beerService;
        private readonly IUserService _userService;


        public ReviewsController(IReviewService reviewService, IMapper mapper,
            IBeerService beerService, IUserService userService)
        {
            _reviewService = reviewService;
            _mapper = mapper;
            _beerService = beerService;
            _userService = userService;
        }

        // GET: Reviews
        public IActionResult Index()
        {
            var reviews = _reviewService.GetAllReviews();
            var reviewsVM = reviews.Select(r => _mapper.Map<ReviewViewModel>(r));
            return View(reviewsVM);
        }

        // GET: Reviews/Details/5
        public IActionResult Details(int id)
        {


            var review = _reviewService.GetReview(id);

            var reviewVM = _mapper.Map<ReviewViewModel>(review);



            return View(reviewVM);
        }

        // GET: Reviews/Create
        public IActionResult Create()
        {
            var beersDTO = _beerService.GetAllBeers();
            var beersVM = beersDTO.Select(x => _mapper.Map<BeerViewModel>(x));


            var usersDTO = _userService.GetAllUsers();
            var usersVM = usersDTO.Select(x => _mapper.Map<UserViewModel>(x));


            ViewData["BeerId"] = new SelectList(beersVM, "BeerId", "BeerName");
            ViewData["UserId"] = new SelectList(usersVM, "UserId", "UserName");

            return View();
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("ReviewId,Text,UserId,BeerId,BeerName,UserName")] ReviewViewModel review)
        {
            try {

                var reviewDTO = _mapper.Map<ReviewDTO>(review);
                var newReviewDTO = _reviewService.CreateReview(reviewDTO);

                var newReviewVM = _mapper.Map<ReviewViewModel>(newReviewDTO);

                //var beersDTO = _beerService.GetAllBeers();
                //var beersVM = beersDTO.Select(x => _mapper.Map<BeerViewModel>(x));


                //var usersDTO = _userService.GetAllUsers();
                //var usersVM = usersDTO.Select(x => _mapper.Map<UserViewModel>(x));

                //ViewData["BeerId"] = new SelectList(beersVM, "BeerId", "BeerName", review.BeerName);
                //ViewData["UserId"] = new SelectList(usersVM, "UserId", "UserName", review.UserName);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception) {

                throw new Exception();
            }
        }

        // GET: Reviews/Edit/5
        public IActionResult Edit(int id)
        {


            var review = _reviewService.GetReview(id);

            if (review == null) {
                return NotFound();
            }
            var beersDTO = _beerService.GetAllBeers();
            var beersVM = beersDTO.Select(x => _mapper.Map<BeerViewModel>(x));


            var usersDTO = _userService.GetAllUsers();
            var usersVM = usersDTO.Select(x => _mapper.Map<UserViewModel>(x));

            //ViewData["BeerId"] = new SelectList(beersVM, "BeerId", "BeerName" ,review.BeerId);
            //ViewData["UserId"] = new SelectList(usersVM, "UserId", "UserName", review.UserId);
            ViewData["BeerId"] = new SelectList(beersVM, "BeerId", "BeerName");
            ViewData["UserId"] = new SelectList(usersVM, "UserId", "UserName");

            var reviewVM = _mapper.Map<ReviewViewModel>(review);
            return View(reviewVM);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("ReviewId,Text,Liked,Disliked,IsDeleted,Flagged,BeerId,UserId")] ReviewViewModel review)
        {
            if (id != review.ReviewId) {
                return NotFound();
            }

            try {

                var reviewDTO = _mapper.Map<ReviewDTO>(review);
                _reviewService.UpdateReview(id, reviewDTO);

                var beersDTO = _beerService.GetAllBeers();
                var beersVM = beersDTO.Select(x => _mapper.Map<BeerViewModel>(x));


                var usersDTO = _userService.GetAllUsers();
                var usersVM = usersDTO.Select(x => _mapper.Map<UserViewModel>(x));

                ViewData["BeerId"] = new SelectList(beersVM, "BeerId", "BeerName", review.BeerId);
                ViewData["UserId"] = new SelectList(usersVM, "Id", "Name", review.UserId);

                return RedirectToAction(nameof(Index));

            }
            catch (DbUpdateConcurrencyException) {
                throw new Exception();
            }
            return RedirectToAction(nameof(Index));

        }

        // GET: Reviews/Delete/5
        public IActionResult Delete(int id)
        {
            if (id == null) {
                return NotFound();
            }

            var review = _reviewService.GetReview(id);

            var reviewVM = _mapper.Map<ReviewViewModel>(review);
            if (review == null) {
                return NotFound();
            }

            return View(reviewVM);
        }

        // POST: Reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var review = _reviewService.DeleteReview(id);


            return RedirectToAction(nameof(Index));

        }


    }
}
