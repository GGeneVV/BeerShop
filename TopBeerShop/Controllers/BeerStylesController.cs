﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.Providers.Contracts;
using TopBeerShop.Web.Models;

namespace TopBeerShop.Web.Controllers
{
    public class BeerStylesController : Controller
    {       
        private readonly IMapper _mapper;
        private readonly IBeerStyleService _beerStyleService;
        private readonly IBeerService _beerService;

        public BeerStylesController(IBeerStyleService beerStyleService, IMapper mapper, IBeerService beerService)
        {
            _beerStyleService = beerStyleService;
            _mapper = mapper;
            _beerService = beerService;
        }

        // GET: BeerStyles
        public IActionResult Index()
        {
            var beerStylesVM = _beerStyleService.GetAllBeerStyles().Select(bs => _mapper.Map<BeerStyleViewModel>(bs));

            return View(beerStylesVM);
        }

        // GET: BeerStyles/Details/5
        public IActionResult Details(int id)
        {
            try {
                var beerStyle = _beerStyleService.GetBeerStyle(id);
                var beerStyleVM = _mapper.Map<BeerStyleViewModel>(beerStyle);

                return View(beerStyleVM);
            }
            catch (Exception) {

                throw new Exception();
            }
        }

        // GET: BeerStyles/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: BeerStyles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Create")]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BeerStyleViewModel beerStyleVM)
        {
            try {

                var styleDTO = _mapper.Map<BeerStyleDTO>(beerStyleVM);
                var newStyleDTO = _beerStyleService.CreateBeerStyle(styleDTO);

                return RedirectToAction(nameof(Index));

            }
            catch (Exception) {

                throw new Exception();
            }
        }

        // GET: BeerStyles/Edit/5
        [HttpGet]
        [Authorize]
        public IActionResult Edit(int id)
        {
            var beerStyle = _beerStyleService.GetBeerStyle(id);
            var beerStyleVM = _mapper.Map<BeerStyleViewModel>(beerStyle);
            return View(beerStyleVM);
        }

        // POST: BeerStyles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public IActionResult Edit(BeerStyleViewModel beerStyleVM)
        {
            try {
                var beerStyleDTO = _mapper.Map<BeerStyleDTO>(beerStyleVM);
                _beerStyleService.EditBeerStyle(beerStyleDTO);

                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateConcurrencyException) {
                throw new Exception();
            }
        }

        // GET: BeerStyles/Delete/5
        [Authorize]
        public IActionResult Delete(int id)
        {
            var beerStyle = _beerStyleService.GetBeerStyle(id);
            var beerStyleVM = _mapper.Map<BeerStyleViewModel>(beerStyle);
            return View(beerStyleVM);
        }

        // POST: BeerStyles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public IActionResult Delete(BeerStyleViewModel beerStyleVM)
        {
            var beerStyleDTO = _mapper.Map<BeerStyleDTO>(beerStyleVM);
            _beerStyleService.EditBeerStyle(beerStyleDTO);
            return RedirectToAction(nameof(Index));
        }

   //     // GET: BeerStyles/AddBeer/5
   //     public IActionResult AddBeerToBeerStyle(int id)
   //     {
   //         var beerStyle = _beerStyleService.GetBeerStyle(id);
   //         var beerStyleVM = _mapper.Map<BeerStyleViewModel>(beerStyle);

   //         var beersDTO = _beerService.GetAllBeers();
   //         var beerVM = beersDTO.Select(x => _mapper.Map<BeerViewModel>(x));
   //         ViewData["BeerId"] = new SelectList(beerVM,"Id",  "BeerName");

   //         return View();
   //     }

   //     // POST: BeerStyles/Delete/5
   //     [HttpPost, ActionName("AddBeerToBeerStyle")]
   //     [ValidateAntiForgeryToken]
   //     public IActionResult AddBeerToBeerStyle(int beerId, BeerStyleViewModel beerStyleVM)
   //     {
   //         var beerStyleDTO = _mapper.Map<BeerStyleDTO>(beerStyleVM);
   ////         _beerStyleService.AddBeerToBeerStyle( beerStyleDTO);
   //         return RedirectToAction(nameof(Index));
   //     }



        private bool BeerStyleExists(int id)
        {
            try {
                _beerStyleService.GetBeerStyle(id);
                return true;
            }
            catch (Exception) {

                return false;
            }
        }
    }
}
