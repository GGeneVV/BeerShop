﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TopBeerShop.Data;
using TopBeerShop.Models;

namespace TopBeerShop.Web.Controllers
{
    public class WishListsController : Controller
    {
        private readonly AppDbContext _context;

        public WishListsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: WishLists
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.WishLists.Include(w => w.Beers).Include(w => w.Users);
            return View(await appDbContext.ToListAsync());
        }

        // GET: WishLists/Details/5
        public async Task<IActionResult> Details(int beerId, int userId)
        {
            if (beerId == null || userId == null) {
                return NotFound();
            }

            var wishList = await _context.WishLists
                .Include(w => w.Beers)
                .Include(w => w.Users)
                .FirstOrDefaultAsync(w => w.BeersId == beerId && w.UserId == userId);
            if (wishList == null) {
                return NotFound();
            }

            return View(wishList);
        }

        // GET: WishLists/Create
        public IActionResult Create()
        {
            ViewData["BeersId"] = new SelectList(_context.Beers, "BeerId", "BeerName");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name");
            return View();
        }

        // POST: WishLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("WishListId,WishListName,UserId,BeersId,Description,CreatedOn,DeletedOn,IsDeleted")] WishList wishList)
        {
            if (ModelState.IsValid) {
                _context.Add(wishList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BeersId"] = new SelectList(_context.Beers, "BeerId", "BeerName", wishList.BeersId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name", wishList.UserId);
            return View(wishList);
        }

        // GET: WishLists/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) {
                return NotFound();
            }

            var wishList = await _context.WishLists.FindAsync(id);
            if (wishList == null) {
                return NotFound();
            }
            ViewData["BeersId"] = new SelectList(_context.Beers, "BeerId", "BeerName", wishList.BeersId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name", wishList.UserId);
            return View(wishList);
        }

        // POST: WishLists/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("WishListId,WishListName,UserId,BeersId,Description,CreatedOn,DeletedOn,IsDeleted")] WishList wishList)
        {
            //if (id != wishList.WishListId) {              COMMENT TO BILD MUST BE UNCOMMENT
            //    return NotFound();
            //}

            if (ModelState.IsValid) {
                try {
                    _context.Update(wishList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException) {
                    //if (!WishListExists(wishList.WishListId)) {               COMMENT TO BILD MUST BE UNCOMMENT
                    //    return NotFound();
                    //}
                    //else {
                    //    throw;
                    //}
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BeersId"] = new SelectList(_context.Beers, "BeerId", "BeerName", wishList.BeersId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Name", wishList.UserId);
            return View(wishList);
        }

        // GET: WishLists/Delete/5
        public async Task<IActionResult> Delete(int beerId, int userId)
        {
            if (beerId == null || userId == null) {
                return NotFound();
            }

            var wishList = await _context.WishLists
                .Include(w => w.Beers)
                .Include(w => w.Users)
                .FirstOrDefaultAsync
                (w => w.BeersId == beerId && w.UserId == userId);
            if (wishList == null) {
                return NotFound();
            }

            return View(wishList);
        }

        // POST: WishLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var wishList = await _context.WishLists.FindAsync(id);
            _context.WishLists.Remove(wishList);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WishListExists(int beerId, int userId)
        {
            return _context.WishLists.Any(w => w.BeersId == beerId && w.UserId == userId);
        }
    }
}
