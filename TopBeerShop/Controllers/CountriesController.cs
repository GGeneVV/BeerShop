﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Web.Models;

namespace TopBeerShop.Web.Controllers
{
    public class CountriesController : Controller
    {
        private readonly ICountryService _service;
        private readonly IMapper _mapper;

        public CountriesController(ICountryService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: Countries
        public IActionResult Index()
        {
            var modelDTO = _service.GetAllCountries();
            var modelMV = modelDTO.Select(x => _mapper.Map<CountryViewModel>(x));

            return View(modelMV);
        }

        // GET: Countries/Details/5
        public IActionResult Details(int id)
        {
            try {
                var country = _service.GetCountry(id);

                if (country == null) {
                    return NotFound();
                }

                var countryViewMOdel = _mapper.Map<CountryViewModel>(country);

                return View(countryViewMOdel);

            }
            catch (Exception) {

                throw new ArgumentException();
            }
        }

        //// GET: Countries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Countries/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CountryViewModel countryViewModel) //WORKS
        {
            try {
                var countryDTO = _mapper.Map<CountryDTO>(countryViewModel);

                var country = _service.CreateCountry(countryDTO);

                var modelMV = _mapper.Map<CountryViewModel>(countryDTO);

               return RedirectToAction(nameof(Index));
                //return View(modelMV);
                //return RedirectToAction("Details", new { id = country.Id });
            }
            catch (Exception) {

                throw;
            }
        }

        // GET: Countries/Edit/5
        [HttpGet]
        public IActionResult Edit(int id)
        {
            var country = _service.GetCountry(id);

            var countryVM = _mapper.Map<CountryViewModel>(country);
          
            return View(countryVM);
        }

        // POST: Countries/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CountryViewModel model)
        {
            try {
                var countryDTO = _mapper.Map<CountryDTO>(model);

                _service.Edit(countryDTO);

                var countryVM = _mapper.Map<CountryViewModel>(countryDTO);

                return RedirectToAction("Details", new { id = countryVM.Id });
            }


            catch (Exception) {

                throw;
            }
        }



        // GET: Countries/Delete/5
        public IActionResult Delete(int id)
        {
            var country = _service.GetCountry(id);

            var countryVM = _mapper.Map<CountryViewModel>(country);


            return View(countryVM);
           // return View(countryVM);
        }

        // POST: Countries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(CountryViewModel country)
        {

            _service.DeleteCountry(country.Id);

            return RedirectToAction(nameof(Index));
        }

       
    }
}
