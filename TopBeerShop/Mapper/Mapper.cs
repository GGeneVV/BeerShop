﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TopBeerShop.Models;
using TopBeerShop.Services.DTO;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Web.Models;

namespace TopBeerShop.Web.Mapper
{
    public class Mapper : Profile
    {
        public Mapper()
        {

            CreateMap<CountryDTO, CountryViewModel>().ReverseMap();

            CreateMap<Country, CountryDTO>().ReverseMap();

            CreateMap<BeerDTO, BeerViewModel>().ReverseMap();

            CreateMap<ReviewDTO, ReviewViewModel>().ReverseMap();
            

            CreateMap<BeerDTO, HomeIndexViewModel>().ReverseMap();

            CreateMap<BeerViewModel, HomeIndexViewModel>().ReverseMap();

            CreateMap<BreweryViewModel, HomeIndexViewModel>().ReverseMap();

            CreateMap<BreweryViewModel, BreweryDTO>().ReverseMap();

            CreateMap<BreweryDTO, BreweryViewModel>().ReverseMap();

            CreateMap<BeerStyleDTO, BeerStyleViewModel>().ReverseMap();



            CreateMap<UserDTO, UserViewModel>().ReverseMap();


        }
    }

}



