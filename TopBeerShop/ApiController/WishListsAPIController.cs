﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using TopBeerShop.Data;
using TopBeerShop.Models;

namespace TopBeerShop.Web.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class WishListsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public WishListsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/WishLists
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WishList>>> GetWishLists()
        {
            return await _context.WishLists.ToListAsync();
        }

        // GET: api/WishLists/5
        [HttpGet("{id}")]
        public async Task<ActionResult<WishList>> GetWishList(int id)
        {
            var wishList = await _context.WishLists.FindAsync(id);

            if (wishList == null) {
                return NotFound();
            }

            return wishList;
        }

        // PUT: api/WishLists/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWishList(int id, WishList wishList)
        {
            //if (id != wishList.WishListId) {
            //    return BadRequest();
            //}

            _context.Entry(wishList).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) {
                //if (!WishListExists(id)) {
                //    return NotFound();                    COMMENT JUST TO BILD 
                //}                                         MUST BE UNCOMMENT AND WORKING
                //else {
                //    throw;
                //}
            }

            return NoContent();
        }

        // POST: api/WishLists
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<WishList>> PostWishList(WishList wishList)
        {
            _context.WishLists.Add(wishList);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetWishList", new { wishList });
        }

        // DELETE: api/WishLists/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<WishList>> DeleteWishList(int id)
        {
            var wishList = await _context.WishLists.FindAsync(id);
            if (wishList == null) {
                return NotFound();
            }

            _context.WishLists.Remove(wishList);
            await _context.SaveChangesAsync();

            return wishList;
        }

        private bool WishListExists(int beerId, int userId)
        {
            return _context.WishLists.Any(e => e.BeersId == beerId && e.UserId == userId);
        }
    }
}
