﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.DTO;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Web.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService _service;

        public ReviewsController(IReviewService service)
        {
            _service = service;
        }

        // GET: api/Reviews
        [HttpGet]
        public ActionResult<ICollection<ReviewDTO>> GetReviews()
        {
            var reviews = _service.GetAllReviews();

            return Ok(reviews);
        }

        // GET: api/Reviews/5
        [HttpGet("{id}")]
        public ActionResult<ReviewDTO> GetReview(int id)
        {
            try {
                var review = _service.GetReview(id);
                return review;
            }
            catch (Exception) {

                throw;
            }
        }

        // PUT: api/Reviews/5

        [HttpPut("{id}")]
        public IActionResult PutReview(int id, ReviewDTO review)
        {
            try {
                _service.UpdateReview(id, review);
            }
            catch (Exception) {

                throw new ArgumentException();
            }

            return Ok(review);

        }

        // POST: api/Reviews

        [HttpPost]
        public ActionResult<ReviewDTO> PostReview(ReviewDTO review)
        {
            try {
                _service.CreateReview(review);

                return CreatedAtAction("GetReview", new { id = review.ReviewId }, review);
            }
            catch (Exception) {

                throw new ArgumentNullException();
            }


        }

        // DELETE: api/Reviews/5
        [HttpDelete("{id}")]
        public ActionResult<Review> DeleteReview(int id)
        {
            try {

                _service.DeleteReview(id);

                return Ok(true);
            }
            catch (Exception) {

                throw new ArgumentException();
            }

        }


    }
}
