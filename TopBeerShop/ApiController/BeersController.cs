﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Web.ApiController
{
    [Route("api/beers")]
    [ApiController]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService _service;

        public BeersController(IBeerService service)
        {
            _service = service;
        }

        // GET: api/Beers
        [HttpGet]
        public ActionResult<ICollection<BeerDTO>> GetBeers()
        {
            return Ok(_service.GetAllBeers());
        }

        // GET: api/Beers/5
        [HttpGet("{id}")]
        public ActionResult<BeerDTO> GetBeer(int id)
        {
            var beer =  _service.GetBeer(id);

            if (beer == null)
            {
                return NotFound();
            }

            return beer;
        }

        // PUT: api/Beers/5
        
        [HttpPut("{id}")]
        public IActionResult PutBeer([FromBody] BeerDTO beer)
        {

            _service.UpdateBeer(beer);


            return Ok(beer);
        }

        // POST: api/Beers
        
        [HttpPost]
        public ActionResult<BeerDTO> PostBeer([FromBody]BeerDTO beer)
        {
            _service.CreateBeer(beer);
            

            return Ok(beer);
        }

        // DELETE: api/Beers/5
        [HttpDelete("{id}")]
        public ActionResult<BeerDTO> DeleteBeer(int id)
        {
            _service.DeleteBeer(id);

            return Ok($"Beer with {id} deleted");
        }

        private bool BeerExists(int id)
        {
            var beer = _service.GetBeer(id);

            return true;
        }
    }
}
