﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Web.Models;

namespace TopBeerShop.Web.ApiController
{
    [Route("api/brewery")]
    [ApiController]
    public class BreweriesAPIController : ControllerBase
    {
        private readonly IBreweryService _service;

        public BreweriesAPIController(IBreweryService service)
        {
            _service = service;
        }

        // GET api/brewery
        [HttpGet]
        public ActionResult<ICollection<BreweryDTO>> Get()
        {
            var breweries = _service
                .GetAllBreweries();

            if (breweries.Count == 0) {
                return Ok("There is no breweries in database!");
            }
            else {
                return Ok(breweries);
            }

        }

        //GET api/brewery/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try {
                var brewery = this._service.GetBrewery(id);
                return Ok(brewery);
            }
            catch (Exception) {
                return this.NotFound("Our database doesn't contain Brewery with such Id!" +
                    Environment.NewLine +
                    "You can try to create it!"); //new Exception();
            }
        }


        //POST api/brewery
        [HttpPost()]
        public IActionResult Post([FromBody] BreweryViewModel model)
        {
            if (!ModelState.IsValid) {
                return BadRequest();
            }

            var breweryDTO = new BreweryDTO {
                Name = model.Name,
                Address = model.Address,
                CountryName = model.CountryName,
                Website = model.Website
            };

            var brewery = _service.CreateBrewery(breweryDTO);

            return Created("post", brewery);
        }


        [HttpDelete]
        public ActionResult<BreweryDTO> Delete([FromBody] BreweryViewModel model)
        {     
            if (!ModelState.IsValid) {
                return NotFound();
            }

            var breweryDTO = new BreweryDTO {
                BreweryId = model.BreweryId,
                Name = model.Name,
                Address = model.Address,
                CountryName = model.CountryName,
                Website = model.Website
            };

            var brewery = _service.DeleteBrewery(breweryDTO.BreweryId);
            return Ok(brewery);
        }


        //PUT api/brewery/:id
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] BreweryViewModel model)
        {
            if (id < 1 || model == null) {
                return BadRequest();
            }

            var breweryDTO = new BreweryDTO {
                Name = model.Name,
            };


            var updatedBreweryDTO = _service.UpdateBrewery(breweryDTO);

            return Ok(updatedBreweryDTO);
        }






    }
}



