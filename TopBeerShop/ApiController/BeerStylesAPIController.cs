﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Web.ApiController
{

    [Route("api/beerStyle")]
    [ApiController]
    public class BeerStylesAPIController : ControllerBase
    {
        private readonly IBeerStyleService _service;

        public BeerStylesAPIController(IBeerStyleService service)
        {
            _service = service;
        } 
        // GET: api/BeerStyles
        [HttpGet]
        public ActionResult<ICollection<BeerStyleDTO>> GetBeerStyles()
        {
            return Ok(_service.GetAllBeerStyles());
        }

        // GET api/<BeerStylesController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/beerstyles
        [HttpPost]
       public  ActionResult<BeerStyleDTO> PostBeerStyle([FromBody] BeerStyleDTO beerStyleDTO)
        {
            _service.CreateBeerStyle(beerStyleDTO);

            return Ok(beerStyleDTO);
        }

        // PUT api/<BeerStylesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<BeerStylesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
