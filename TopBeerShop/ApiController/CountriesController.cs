﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Web.ApiController
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService _service;

        public CountriesController(ICountryService service)
        {
            _service = service;
        }

        // GET: api/Countries
        [HttpGet]
        public ActionResult<ICollection<CountryDTO>> GetCountries()
        {
            var countries = _service.GetAllCountries();

            return RedirectToAction();

        }

        // GET: api/Countries/5
        [HttpGet("{id}")]
        public ActionResult<CountryDTO> GetCountry(int id)
        {
            var country = _service.GetCountry(id);

            if (country == null)
            {
                return NotFound();
            }

            return country;
        }

        // PUT: api/Countries/5
        
        [HttpPut("{id}")]
        public IActionResult PutCountry(CountryDTO country)
        {
            try {
                _service.Edit(country);
            }
            catch (Exception) {

                throw new ArgumentException();
            }

            return Ok(country);
        }

        // POST: api/Countries
        
        [HttpPost]
        public ActionResult<CountryDTO> PostCountry(CountryDTO country)
        {
            try {
                _service.CreateCountry(country);
            }
            catch (Exception) {

                throw;
            }
           

            return CreatedAtAction("GetCountry",  country);
        }

        // DELETE: api/Countries/5
        [HttpDelete("{id}")]
        public ActionResult<Country> DeleteCountry(int id)
        {
            try {
                _service.DeleteCountry(id);
            }
            catch (Exception) {

                throw new ArgumentException();
            }
            return Ok();
        }

        private bool CountryExists(int id)
        {
            var exist = _service.GetCountry(id);

            return true;
        }
    }
}
