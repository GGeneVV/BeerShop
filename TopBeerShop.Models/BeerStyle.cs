﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TopBeerShop.Models.Abstract;

namespace TopBeerShop.Models
{
    public class BeerStyle :Entity
    {
        [Key]
        public int StyleId { get; set; }
        [MinLength(3), MaxLength(30), ]
        public string Style { get; set; }

        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}
