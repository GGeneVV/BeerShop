﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TopBeerShop.Models.Abstract;

namespace TopBeerShop.Models
{
   public class User : IdentityUser<int>
    {
        [Key]

        public int UserId { get; set; }

        [Required(ErrorMessage = "User Name is required")]
        [StringLength(100)]
        public string Name { get; set; }
      
        public bool IsDeleted { get; set; }
        public bool IsBanned { get; set; }

        public string BanDescription { get; set; }
 
       
        public ICollection<Review> UserReviews { get; set; } = new List<Review>();

        public ICollection<WishList> WishLists { get; set; } = new List<WishList>();

        public DrankList DrinkedBeers { get; set; }         //toDO thist must be collection???
    }
}
