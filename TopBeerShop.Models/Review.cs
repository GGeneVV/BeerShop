﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace TopBeerShop.Models
{
    public class Review
    {
        [Key]
        public int ReviewId {get;set;}

        public int Rating { get; set; }
             
        public string Text { get; set; }
        public bool Liked { get; set; }

        public bool Disliked { get; set; }

        public bool IsDeleted { get; set; }

        public bool Flagged { get; set; }

        public int BeerId { get; set; }

        public Beer Beer { get; set; } 
      
        public int UserId { get; set; }
        public User User { get; set; }

        
    }
}
