﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models.Abstract;

namespace TopBeerShop.Models
{
    public class WishList : Entity
    {
        public int BeersId { get; set; }

        public int UserId { get; set; }

        public Beer Beers { get; set; }
        public User Users { get; set; }       

    }
}
