﻿using TopBeerShop.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TopBeerShop.Models
{
    public class Beer : Entity
    {
      

        [Key]
        public int BeerId { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string BeerName { get; set; }
        [Required]
        public double ABV { get; set; }
        [Required]
        public double Price { get; set; }
       
        public bool IsBeerOfTheWeek { get; set; }
       
        public int? BeerStyleId { get; set; }
        
        public  BeerStyle BeerStyles { get; set; }

        public int? BreweryId { get; set; }
       
        public Brewery Brewery{get;set;}
        public ICollection<Review> Reviews { get; set; } = new List<Review>();

        public ICollection<WishList> WishLists { get; set; } = new List<WishList>();

       

        





    }
}
