﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TopBeerShop.Models
{
    public class DrankList
    {
        [Key]
        public int Id { get; set; }

        public int BeerId { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
    }
}

//[drank list]  {int beerId, beer}
