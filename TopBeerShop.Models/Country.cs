﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models.Abstract;

namespace TopBeerShop.Models
{
    public class Country : Entity
    {
        
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Brewery> Breweries { get; set; } = new List<Brewery>();
    }
}
