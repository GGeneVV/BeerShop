﻿using TopBeerShop.Models;
using TopBeerShop.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;

namespace TopBeerShop.Models
{
    public class Brewery : Entity
    {
        [Key]
        public int BreweryId { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 3)]
        public string Name { get; set; }    

        [StringLength(100 , MinimumLength =5)]
        public string Website { get; set; }

        [StringLength(100 , MinimumLength =5)]
        public string Address { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
        public int CountryId { get; set;}
        public Country Country { get; set; }
    }
}
