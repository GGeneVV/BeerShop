﻿using TopBeerShop.Models;
using TopBeerShop.Models.Abstract;

namespace Beers.Data.Models.Abstract
{
    public abstract class BeerFeedback : Entity
    {
        public int BeerId { get; set; }
        public Beer Beer { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
