﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTO;

namespace TopBeerShop.Services.MappersDTO
{
    public static class ReviewDTOMapper
    {
        public static ReviewDTO GetDTO(this Review item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new ReviewDTO
            {
                ReviewId = item.ReviewId,

                Rating = item.Rating,

                Text = item.Text,

                Liked = item.Liked,

                Disliked = item.Disliked,

                IsDeleted = item.IsDeleted,

                Flagged = item.Flagged,

                UserName = item.User.Name,
                 
                

                BeerName = item.Beer.BeerName,

                BeerId = item.BeerId,

                UserId = item.UserId
            };
        }

        public static ICollection<ReviewDTO> GetDTO(this ICollection<Review> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
