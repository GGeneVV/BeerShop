﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Services.MappersDTO
{
    public static class BreweryDTOMapper
    {
        public static BreweryDTO GetDTO(this Brewery item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BreweryDTO
            {
                BreweryId = item.BreweryId,
                Name = item.Name,
                Website = item.Website,
                Address = item.Address,
                Beers = item.Beers,
                CountryId = item.CountryId,
                CountryName = item.Country.Name            
            };
        }
        public static ICollection<BreweryDTO> GetDTO(this ICollection<Brewery> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
