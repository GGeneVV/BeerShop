﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Services.MappersDTO
{
    public static class BeerStyleDTOMapper
    {
        public static BeerStyleDTO GetDTO(this BeerStyle item)
        {
            if (item== null)
            {
                throw new ArgumentNullException();
            }

            return new BeerStyleDTO
            {
                Id = item.StyleId,
                Style = item.Style,
                Description = item.Description
            };
        }

        public static ICollection<BeerStyleDTO> GetDTO (this ICollection<BeerStyle> item)
        {
            return item.Select(GetDTO).ToList();
        }

    }
}
