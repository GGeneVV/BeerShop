﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;

namespace TopBeerShop.Services.MappersDTO
{
    public static class UserDTOMapper
    {
        public static UserDTO GetDTO(this User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new UserDTO
            {
                UserId = item.Id,
                UserName = item.Name,
                IsBanned = item.IsBanned,
                BanDescription = item.BanDescription,
                UserReviews = item.UserReviews,
                WishLists = item.WishLists,
                
            };
        }
        public static ICollection<UserDTO> GetDTO(this ICollection<User> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
