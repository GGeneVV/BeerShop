﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTO;

namespace TopBeerShop.Services.MappersDTO
{
    public static class WishListDTOMapper
    {
        public static WishListDTO GetDTO(this WishList wishList)
        {
            if (wishList == null)
            {
                throw new ArgumentNullException();
            }
            return new WishListDTO
            {
   // TODO            WishListName = wishList.WishListName,         
                BeersId = wishList.BeersId,
                UserId = wishList.UserId,
   //   TODO          WishListId = wishList.WishListId
            };
        }

        public static ICollection<WishListDTO> GetDTO(this ICollection<WishList> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}


