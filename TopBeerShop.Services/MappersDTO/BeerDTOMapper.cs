﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Services.MappersDTO
{
    public static class BeerDTOMapper
    {
        public static BeerDTO GetDTO(this Beer item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BeerDTO
            {
                BeerId = item.BeerId,
                BeerName = item.BeerName,
                ABV = item.ABV,
                StyleId = item.BeerStyleId,
                BreweryId = item.BreweryId,
                Price = item.Price,
                CreatedOn = DateTime.UtcNow

            };
        }
        public static ICollection<BeerDTO> GetDTO(this ICollection<Beer> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}



