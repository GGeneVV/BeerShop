﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Services.DTOs
{
    public class BreweryDTO
    {
        public int BreweryId { get; set; }
        public string Name { get; set; }      
        public string Website { get; set; }
        public string Address { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
        public int CountryId { get; set; }
        public string CountryName { get; set; } 
    }
}
