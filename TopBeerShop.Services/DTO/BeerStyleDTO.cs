﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Services.DTOs
{
    public class BeerStyleDTO
    {
        public int Id { get; set; }
        public string Style { get; set; }
        public string Description { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();        
    }
}
