﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Services.DTO
{
   public class ReviewDTO
    {
        public int ReviewId { get; set; }

        public int Rating { get; set; }

        public string Text { get; set; }
        public bool Liked { get; set; }
        public bool Disliked { get; set; }

        public bool IsDeleted { get; set; }

        public bool Flagged { get; set; }
        public int BeerId { get; set; }
        public int UserId { get; set; }
       public string UserName { get; set; }
       public string BeerName { get; set; }

        
        
        

    }
}
