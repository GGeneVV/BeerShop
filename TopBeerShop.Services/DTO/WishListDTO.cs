﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TopBeerShop.Services.DTO
{
    public class WishListDTO
    {
        public int WishListId { get; set; }

        public string WishListName { get; set; }

        public int UserId { get; set; }

        
        public int BeersId { get; set; }
        
    }
}
