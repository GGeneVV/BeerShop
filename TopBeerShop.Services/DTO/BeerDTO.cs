﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Services.DTOs
{
    public class BeerDTO
    {
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public double ABV { get; set; }
        public double Price { get; set;}

        public bool IsBeerOfTheWeek { get; set; }
    
        public int? StyleId { get; set; }
       
        public int? BreweryId { get; set; }

       
        public DateTime CreatedOn { get; set; }


    }
}
