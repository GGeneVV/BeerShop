﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Services.DTOs
{
    public class UserDTO
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsBanned { get; set; }
        public string BanDescription { get; set; }
        public ICollection<WishList> WishLists { get; set; } = new List<WishList>();
        public ICollection<Review> UserReviews { get; set; } = new List<Review>();
    }
}
