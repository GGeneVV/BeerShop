﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTO;
using TopBeerShop.Services.MappersDTO;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Services.Services
{
    public class WishListService : IWishListService
    {
        private readonly AppDbContext _context;
        
        private readonly IBeerService _beerService;
        private readonly IUserService _userService;
        
        public WishListService(AppDbContext context, IBeerService beerService
          ) ////IUserService _userService;
        {
            _context = context;
           
            _beerService = beerService;
           
        }
        public WishListDTO CreateWishList(WishListDTO wishListDTO)
        {
            try
            {

               
                var wishList = new WishList
                {
                    //WishListName = wishListDTO.WishListName,
                    
                    // UserId = _userService.GetUserId(reviewDTO.UserName),
                };

                _context.WishLists.Add(wishList);
                _context.SaveChanges();


                return wishListDTO;

            }
            catch (Exception)
            {

                throw new ArgumentException();
            }
        }

        public bool DeleteWishList(int id)
        {
            try
            {

                var wishList = _context.WishLists
                    .Where(x => x.IsDeleted == false)
                    .Include(b => b.Beers)
                    .Include(s => s.Users)
                    .FirstOrDefault(x => x.UserId == id );                      //TODO

                if (wishList == null)
                {
                    throw new ArgumentNullException();
                }

                wishList.IsDeleted = true;

                _context.SaveChanges();

                return wishList.IsDeleted;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }

        public ICollection<WishListDTO> GetAllWishLists()
        {
            try
            {
                var wishLists = _context.WishLists
                   .Where(w => w.IsDeleted == false)
                   .Include(b => b.Beers)
                    .Include(s => s.Users)
                    .Select(w => w.GetDTO()).ToList();

                return wishLists;

            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }

        public WishListDTO GetWishList(int id)
        {
            try
            {

                var wishList = _context.WishLists
                    .Where(w => w.IsDeleted == false)
                    .Include(b => b.Beers)
                    .Include(u => u.Users) 
                    .FirstOrDefault(w => w.UserId == id).GetDTO();            //TODO

                if (wishList == null)
                {
                    throw new ArgumentNullException();
                }

               
                return wishList;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }

        public int GetWishListId(string name)
        {
            try
            {
                if (name == null)
                {
                    throw new ArgumentNullException();
                }

                var wishList = _context.WishLists
                    .Include(b => b.Beers)
                    .Include(s => s.Users)
                    .FirstOrDefault(b => b.Users.Name == name);//TODO

                if (wishList == null)
                {
                    throw new ArgumentNullException();
                }
                return wishList.UserId;             //TODO;

            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }

        public WishListDTO UpdateWishList(int id, WishListDTO wishListDTO)
        {
            try
            {
               
                var wishList = _context.WishLists
                     .Where(r => r.IsDeleted == false)
                     .Include(b => b.Beers)
                     .Include(u => u.Users)
                     .FirstOrDefault(r => r.UserId == id);              //TODO

             //   wishList.WishListName = wishListDTO.WishListName;              //TODO
                wishList.BeersId = wishListDTO.BeersId;
                wishList.UserId = wishListDTO.UserId;
                

                _context.SaveChanges();

                return wishListDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
