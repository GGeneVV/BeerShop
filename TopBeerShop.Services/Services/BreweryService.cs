﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;

namespace TopBeerShop.Services
{
    public class BreweryService : IBreweryService
    {

             private readonly AppDbContext _context;
             private readonly IMapper _mapper;

        public BreweryService(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ICollection<BreweryDTO> GetAllBreweries()
        {
            var breweries = _context.Breweries
                .Include(br => br.Country)
                .Where(br => br.IsDeleted == false)
                .ToList();

            return breweries.GetDTO();
        }

        
        public BreweryDTO CreateBrewery(BreweryDTO breweryDTO)
        {

            if (breweryDTO.Name == null)
            {
                throw new ArgumentNullException();
            }

            if (_context.Breweries.Where(x => !x.IsDeleted).Where(br => br.Country.Name == breweryDTO.CountryName).Any(br => br.Name == breweryDTO.Name))
            {
                throw new ArgumentException("Brewery with such name already exists!");
            }

            var tempCountry = _context.Countries
                .Where(x => !x.IsDeleted)
                .FirstOrDefault(c => c.Name == breweryDTO.CountryName) ?? throw new ArgumentException
                ("County with this name cannot be found in our database. Pleace, try to create county first!");
            try
            {

                var brewery = new Brewery
                {
                    Name = breweryDTO.Name,
                    Website = breweryDTO.Website,
                    Address = breweryDTO.Address,
                    Country = tempCountry,
                    CountryId = tempCountry.Id,
                    CreatedOn = DateTime.UtcNow,
                };

                if (_context.Breweries
                    .Where(x => x.IsDeleted)
                    .Where(br => br.Country.Name == breweryDTO.CountryName)
                    .Any(br => br.Name == breweryDTO.Name))
                {
                    brewery = _context.Breweries.Where(br => br.Country.Name == breweryDTO.CountryName)
                    .First(br => br.Name == breweryDTO.Name);
                    brewery.IsDeleted = false;

                    _context.Update(brewery);
                    _context.SaveChanges();

                    return breweryDTO;
                }

                _context.Breweries.Add(brewery);
                _context.SaveChanges();

                return breweryDTO;
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during Creating Brewery process! Please check your input and try again");
            }
        }
        public BreweryDTO GetBrewery(int id)
        {
            var brewery = _context.Breweries
                .Include(br => br.Country)
                .Where(br => br.IsDeleted == false)
                .FirstOrDefault(br => br.BreweryId == id);

            if (brewery.Name == null)
            {
                throw new ArgumentNullException("There is no Brewery with such name");
            }

            return brewery.GetDTO();
        }

        public bool DeleteBrewery(int id)
        {
            try
            {
                var brewery = _context.Breweries
                                .Where(br => br.IsDeleted == false)                             
                                .FirstOrDefault(br => br.BreweryId == id);

                if (brewery == null)
                {
                    throw new ArgumentNullException();
                }

                brewery.IsDeleted = true;
                _context.Update(brewery);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public BreweryDTO UpdateBrewery(BreweryDTO breweryDTO)
        {
            try
            {                
                var brewery = _context.Breweries
                .Where(br => br.IsDeleted == false)
                .Include(br => br.Country)
                .FirstOrDefault(br => br.BreweryId == breweryDTO.BreweryId);


                brewery.Name = breweryDTO.Name;
                brewery.Address = breweryDTO.Address;
                brewery.CountryId = breweryDTO.CountryId;
                brewery.Country = _context.Countries
                                    .Where(c=>c.IsDeleted == false)
                                    .FirstOrDefault(c=>c.Id == breweryDTO.CountryId);
             
                _context.Update(brewery);
                _context.SaveChanges();
                return breweryDTO;
            }

            catch (Exception)
            {
                throw new Exception("Something messed up during Updating Brewery process! Please try again.");
            }
        }

        public bool AddBeerToBrewery(BeerDTO beerDTO, BreweryDTO breweryDTO)
        {
            var brewery = _context.Breweries
                .Where(br => br.IsDeleted == false)
                .FirstOrDefault(br => br.BreweryId == breweryDTO.BreweryId);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            try
            {
                var beer = _context.Beers
                    .Where(b => b.IsDeleted == false)
                    .FirstOrDefault(b => b.BeerId == beerDTO.BeerId);

                if (beer == null)
                {
                    throw new ArgumentNullException();
                }

                brewery.Beers.Add(beer);
                _context.Update(brewery);
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during adding Beer to  Brewery process! Please check your input and try again.");
            }
        }

        public int GetId(string name)
        {
            var breweryId = _context.Breweries
                .Where(b => b.IsDeleted == false)
                .FirstOrDefault(b => b.Name == name);

            return breweryId.BreweryId;
        }
    }
}
