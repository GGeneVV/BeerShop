﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Web.Services
{
    public class BeerService : IBeerService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly IBreweryService _breweryService;
        private readonly IBeerStyleService _beerStyleService;
        public BeerService(AppDbContext context , IMapper mapper , 
            IBreweryService breweryService , IBeerStyleService beerStyleService)
        {
            _context = context;
            _mapper = mapper;
            _breweryService = breweryService;
            _beerStyleService = beerStyleService;
        }


        public BeerDTO CreateBeer(BeerDTO beerDTO)
        {
            try
            {

                if (beerDTO == null)
                {
                    throw new ArgumentNullException();
                }

                var beer = new Beer
                {
                    BeerId = beerDTO.BeerId,
                    BeerName = beerDTO.BeerName,
                    ABV = beerDTO.ABV,
                    BeerStyleId = beerDTO.StyleId,
                    BreweryId = beerDTO.BreweryId,
                    Price = beerDTO.Price,
                    CreatedOn = DateTime.UtcNow
                };

                if (_context.Beers.Any(b => b.BeerName == beer.BeerName))
                {
                    throw new ArgumentException("Beer with such name already exists!");
                }

                _context.Beers.Add(beer);
                
                _context.SaveChanges();

                

                return beerDTO;
            }
            catch (Exception)
            {

                throw new ArgumentException();
            }
        }


        public bool DeleteBeer(int id)
        {
            try
            {

                var beer = _context.Beers
                    .Where(x => x.IsDeleted == false)
                    .Include(b => b.Brewery)
                    .Include(s => s.BeerStyles)
                    .FirstOrDefault(x => x.BeerId == id);

                if (beer == null)
                {
                    throw new ArgumentNullException();
                }

                beer.IsDeleted = true;

                _context.SaveChanges();

                return beer.IsDeleted;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }

        public ICollection<BeerDTO> GetAllBeers()
        {
            try
            {
                var beers = _context.Beers
                   .Where(beer => beer.IsDeleted == false)
                   .Include(b => b.Brewery)
                   .Include(s => s.BeerStyles)
                   .Select(beer => beer.GetDTO()).ToList();

                return beers;

            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }



        public BeerDTO GetBeer(int id)
        {
            try
            {

                var beer = _context.Beers
                    .Where(beer => beer.IsDeleted == false)
                    .Include(b => b.Brewery)
                    .Include(s => s.BeerStyles)
                    .Include(r =>r.Reviews)
                    .FirstOrDefault(beer => beer.BeerId == id).GetDTO();

                

                return beer;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }

        public int GetBeerId(string name)
        {
            try
            {
                if (name == null)
                {
                    throw new ArgumentNullException();
                }

                var beer = _context.Beers
                    .Include(b => b.Brewery)
                    .Include(s => s.BeerStyles)
                    .FirstOrDefault(b => b.BeerName == name);

                if (beer == null)
                {
                    throw new ArgumentNullException();
                }
                return beer.BeerId;

            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }


        public BeerDTO UpdateBeer(BeerDTO beerDTO)
        {
            try
            {

                var beer = _context.Beers.Where(x => x.IsDeleted == false)
                    .Include(b => b.Brewery)
                    .Include(s => s.BeerStyles)
                    .FirstOrDefault(x => x.BeerId == beerDTO.BeerId);


                beer.BeerName = beerDTO.BeerName;
                beer.ABV = beerDTO.ABV;
                beer.BeerStyleId = beerDTO.StyleId;
                beer.BreweryId = beerDTO.BreweryId;
                beer.Price = beerDTO.Price;
                   

                _context.Update(beer);
                


                _context.SaveChanges();
               

                return beerDTO;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }

        }


    }
}
