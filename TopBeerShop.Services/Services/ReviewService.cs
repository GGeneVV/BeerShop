﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTO;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Services.Services
{
    public class ReviewService : IReviewService
    {
        private readonly AppDbContext _context;
        private readonly IBeerService _beerService;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public ReviewService(AppDbContext context, IBeerService beerService ,
            IUserService userService , IMapper mapper)
        {
            _context = context;
            _beerService = beerService;
            _userService = userService;
            _mapper = mapper;
        }

        public ReviewDTO CreateReview(ReviewDTO reviewDTO)
        {
  
            try
            {
                //Creates Review but could not bind it to user and beer name / only to their Id`s......

                var user = _context.Users.Where(u => !u.IsDeleted).FirstOrDefault(u => u.Id == reviewDTO.UserId);
                var beer = _context.Beers.Where(b => !b.IsDeleted).FirstOrDefault(b => b.BeerId == reviewDTO.BeerId);

                var review = new Review
                {
                    Rating = reviewDTO.Rating,
                    Text = reviewDTO.Text,
                    BeerId =  reviewDTO.BeerId,                
                    UserId =  reviewDTO.UserId,    
                    User = user,
                    Beer = beer,
               
            };
                

                _context.Reviews.Add(review);
                beer.Reviews.Add(review);
                _context.Beers.Update(beer);
                _context.SaveChanges();


                return review.GetDTO();

               

            }
            catch (Exception)
            {

                throw new ArgumentException();
            }

        }



        public bool DeleteReview(int id)
        {
            try
            {

                var review = _context.Reviews.Where(x => !x.IsDeleted)
                    .Include(u => u.User)
                    .Include(b => b.Beer)
                .FirstOrDefault(x => x.ReviewId.Equals(id));

                review.IsDeleted = true;

                _context.Update(review);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }

        public bool DisLikeReview(int id)
        {
            try
            {

                var review = _context.Reviews.Where(x => !x.Disliked)
                .FirstOrDefault(x => x.ReviewId.Equals(id));

                review.Disliked = true;

                _context.Update(review);
                _context.SaveChanges();

            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }

        public bool FlagReview(int id)
        {
            try
            {

                var review = _context.Reviews.Where(x => !x.Flagged)
                .FirstOrDefault(x => x.ReviewId.Equals(id));

                review.Flagged = true;
                _context.Update(review);
                _context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }

        public ICollection<ReviewDTO> GetAllReviews()
        {
            try
            {
                var reviews = _context.Reviews
                    .Include(u=>u.User)
                    .Include(b=>b.Beer)
                     .Where(r => r.IsDeleted == false)
                      .Select(r => r.GetDTO()).ToList();

                return reviews;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }

        }

        public ReviewDTO GetReview(int id)
        {
            var review = _context.Reviews
                .Where(r => r.IsDeleted == false)
                .FirstOrDefault(r => r.ReviewId == id).GetDTO();

            return review;
        }

        public int GetReviewId(string name)
        {
            try
            {

                var review = _context.Reviews
                    .FirstOrDefault(b => b.Text == name);

                if (review == null)
                {
                    throw new ArgumentNullException();
                }
                return review.ReviewId;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }

        public bool LikeReview(int id)
        {
            try
            {
                var review = _context.Reviews
              .Where(r => r.IsDeleted == false)
               .FirstOrDefault(r => r.ReviewId == id).GetDTO();

                review.Liked = true;

                _context.Update(review);
                _context.SaveChanges();

                return review.Liked;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }
        }



        public ReviewDTO UpdateReview(int id, ReviewDTO reviewDTO)
        {
            try
            {
                var review = _context.Reviews
                     .Where(r => r.IsDeleted == false)
                     .Include(b => b.Beer)
                     .Include(u => u.User)
                     .FirstOrDefault(r => r.ReviewId == id);

                review.Text = reviewDTO.Text;
                review.Liked = reviewDTO.Liked;
                review.Disliked = reviewDTO.Disliked;
                review.Flagged = reviewDTO.Flagged;

                _context.Update(review);
                _context.SaveChanges();
                
                return review.GetDTO();
            }
            catch (Exception)
            {

                throw;
            }
        }



    }
}
