﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;

namespace TopBeerShop.Services
{
    public class CountryService : ICountryService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public CountryService(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }

        public CountryDTO CreateCountry(CountryDTO countryDTO)
        {
            try
            {

                if (countryDTO == null)
                {
                    throw new ArgumentNullException();
                }

                var country = new Country
                {
                    Name = countryDTO.Name,

                };

                if (_context.Countries.Any(b => b.Name == country.Name))
                {
                    throw new ArgumentException("Country with such name already exists!");
                }

                _context.Countries.Add(country);

                _context.SaveChanges();

                var dto = new CountryDTO
                {
                    Id = country.Id,
                    Name = country.Name,

                };


                return dto;
            }
            catch (Exception)
            {

                throw;
            }


        }

        public int GetCountryId(string name)
        {
            try
            {

                var country = _context.Countries
                    .FirstOrDefault(b => b.Name == name);

                if (country == null)
                {
                    throw new ArgumentNullException();
                }
                return country.Id;
            }
            catch (Exception)
            {

                throw new NotImplementedException();
            }

        }


        public bool DeleteCountry(int id)
        {
            try
            {

                var country = _context.Countries.Where(x => !x.IsDeleted)
                    .FirstOrDefault(x => x.Id.Equals(id));

                if (country == null)
                {
                    throw new ArgumentNullException();
                }

                country.IsDeleted = true;

                _context.SaveChanges();

                return country.IsDeleted;
            }
            catch (Exception)
            {

                return false;
            }
        }


        public ICollection<CountryDTO> GetAllCountries()
        {
            try
            {

                var countries = _context.Countries
                    .Where(country => country.IsDeleted == false)
                    .Select(country => country.GetDTO()).ToList();


                return countries;
            }
            catch (Exception)
            {

                throw;
            }
        }




        public CountryDTO GetCountry(int id)
        {
            try
            {
                var country = _context.Countries
                    .Where(country => country.IsDeleted == false)
                    .FirstOrDefault(country => country.Id == id).GetDTO();

                if (country == null)
                {
                    throw new ArgumentNullException();
                }

                return country;

            }
            catch (Exception)
            {

                throw;
            }
        }



        public CountryDTO Edit(CountryDTO countryDTO)
        {
            try
            {

                var country = _context.Countries.FirstOrDefault(x => x.IsDeleted == false && x.Id == countryDTO.Id);


                if (country == null)
                {
                    throw new ArgumentNullException();
                }
                country.Name = countryDTO.Name;

                var dto = _mapper.Map<CountryDTO>(country);

                

                //country.Name = countryDTO.Name;
                //country.Breweries = countryDTO.Breweries; ...... equivalent to the above.
                _context.Update(country);
                _context.SaveChanges();

                return country.GetDTO();

            }
            catch (Exception)
            {

                throw;
            }
        }




    }
}
