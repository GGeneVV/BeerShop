﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.Contracts;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Services.Services
{
    public class UserService : IUserService
    {
        private readonly AppDbContext _context;
        private readonly IBeerService beerService;
        public UserService(AppDbContext context, IBeerService beerService)
        {
            this._context = context;
            this.beerService = beerService;
        }

        public UserDTO BanUser(int id)
        {
            try
            {
                var user = _context.Users.Where(u => !u.IsDeleted)
                    .FirstOrDefault(u => u.UserId == id) ?? throw new ArgumentException
                    ("User with this ID cannot be found!");

                if (user.IsBanned)
                {
                    throw new ArgumentException("This User is already banned!");
                }

                user.IsBanned = true;
                _context.SaveChanges();
                return user.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during Ban User process! Please try again.");
            }
        }

        public UserDTO BanUser(string userName)
        {
            try
            {
                var user = _context.Users.Where(u => u.IsDeleted)
                    .FirstOrDefault(u => u.Name == userName) ?? throw new ArgumentException
                    ("User with this Name cannot be found!");

                if (user.IsBanned)
                {
                    throw new ArgumentException("This User is already banned!");
                }

                user.IsBanned = true;
                _context.SaveChanges();
                return user.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during Ban User process! Please try again.");
            }
        }

        public bool DeleteUser(int id)
        {
            try
            {
                var user = _context.Users
                   .Where(u => !u.IsDeleted)
                    .FirstOrDefault(u => u.UserId == id) ?? throw new ArgumentException
                    ("User with this ID cannot be found!");

                user.IsDeleted = true;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during Delete User process! Please try again.");
            }
        }

        public ICollection<UserDTO> GetAllUsers()
        {
            ICollection<UserDTO> users = _context.Users.Where(u => !u.IsDeleted).ToList().GetDTO();

            return users;
        }

        public string GetName(int id)
        {
            var user = _context.Users
                   .Where(u => !u.IsDeleted)
                    .FirstOrDefault(u => u.UserId == id) ?? throw new ArgumentException
                    ("User with this ID cannot be found!");

            return user.UserName;
        }

        public UserDTO GetUser(string userName)
        {
            var user = _context.Users
               .Where(u => !u.IsDeleted)
                .FirstOrDefault(u => u.UserName == userName) ?? throw new ArgumentException
                ("User with this Name cannot be found!");

            return user.GetDTO();
        }

        public UserDTO GetUser(int id)
        {
            var user = _context.Users
                  .Where(u => !u.IsDeleted)
                   .FirstOrDefault(u => u.UserId == id) ?? throw new ArgumentException
                   ("User with this ID cannot be found!");

            return user.GetDTO();
        }

        public int GetUserId(string name)
        {
            var id = _context.Users.Where(u => !u.IsDeleted)
                  .FirstOrDefault(u => u.Name == name) ?? throw new ArgumentException
                  ("User with this name cannot be found!");
            return id.Id;
        }
    }
}
