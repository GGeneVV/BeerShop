﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using TopBeerShop.Data;
using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Services
{
    public class BeerStyleServices : IBeerStyleService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public BeerStyleServices(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public BeerStyleDTO CreateBeerStyle(BeerStyleDTO beerStyleDTO)
        {
            if (beerStyleDTO == null)
            {
                throw new ArgumentNullException("BeerStyle cannot be null!");
            }

            if (_context.BeerStyles.Where(style => !style.IsDeleted).Any(style => style.Style == beerStyleDTO.Style))
            {
                throw new ArgumentException("BeerStyle with such name already exists!");
            }

            try
            {
                var beerStyle = new BeerStyle
                {
                    StyleId = beerStyleDTO.Id,
                    Style = beerStyleDTO.Style,
                    Description = beerStyleDTO.Description,
                    CreatedOn = DateTime.UtcNow,
                };

                if (_context.BeerStyles
                    .Where(bs => bs.IsDeleted)
                    .Any(bs => bs.Style == beerStyle.Style))
                {
                    beerStyle = _context.BeerStyles.FirstOrDefault(bs => bs.Style == beerStyle.Style);
                    beerStyle.IsDeleted = false;
                    _context.Update(beerStyle);
                    _context.SaveChanges();

                    return beerStyle.GetDTO();
                }

                _context.BeerStyles.Add(beerStyle);
                _context.SaveChanges();

                return beerStyle.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during Creating BeerStyle process! Please check your input and try again");
            }
        }
                
        public BeerStyleDTO EditBeerStyle(BeerStyleDTO beerStyle)
        {
            var style = _context.BeerStyles
                .Where(s => s.IsDeleted == false)
                .FirstOrDefault(s => s.StyleId == beerStyle.Id);

            if (style == null)
            {
                throw new Exception("This Style cannot be found");
             }

            try
            {                
                style.Style = beerStyle.Style;
                style.Description = beerStyle.Description;                
                _context.Update(style);
                _context.SaveChanges();

                return style.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during Edit Beer Style process! Please check your input and try again");
            }
        }

        public ICollection<BeerStyleDTO> GetAllBeerStyles()
        {
            try
            {
                var beerStylesList = _context.BeerStyles
                     .Where(style => style.IsDeleted == false)
                     .ToList();

                if (beerStylesList.Count == 0)
                {
                    throw new ArgumentNullException("There is no BeerStyle in our database!");
                }

                return beerStylesList.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception("Something messed up during GetAll BeerStyle process! Please check your input and try again");
            }
        }

        public int GetId(string name)
        {
            var style = _context.BeerStyles
                .FirstOrDefault(s => s.Style == name);

            if (style == null)
            {
                throw new Exception();
            }

            return style.StyleId;
        }

        public bool AddBeerToBeerStyle(BeerStyleDTO beerStyleDTO, BeerDTO beerDTO)
        {
            var style = _context.BeerStyles
                .FirstOrDefault(s => s.StyleId == beerStyleDTO.Id && s.IsDeleted == false);

            if (style == null)
            {
                throw new Exception();
            }

            try
            {
                var beer = _context.Beers
                .FirstOrDefault(b => b.BeerId == beerStyleDTO.Id && b.IsDeleted == false);

                if (beer == null)
                {
                    throw new Exception();
                }

                style.Beers.Add(beer);
                beer.BeerStyles = style;
                _context.Update(style);
                _context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public BeerStyleDTO DeleteBeerStyle(int id)
        {
            var style = _context.BeerStyles
               .FirstOrDefault(s => s.StyleId == id && s.IsDeleted == false);

            if (style == null)
            {
                throw new Exception();
            }

            style.IsDeleted = true;

            _context.BeerStyles.Update(style);
            _context.SaveChanges();

            return style.GetDTO();
        }

        public BeerStyleDTO GetBeerStyle(int id)
        {
            var style = _context.BeerStyles
                .Where(bs => !bs.IsDeleted)
                .FirstOrDefault(bs => bs.StyleId == id);

            if (style == null)
            {
                throw new Exception("There is no Beer Style with such Id!");
            }

            return style.GetDTO();
        }
    }
}
