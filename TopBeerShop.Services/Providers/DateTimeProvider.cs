﻿
using System;
using TopBeerShop.Services.Providers.Contracts;

namespace TopBeerShop.Services.Providers
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.Now;
    }
}
