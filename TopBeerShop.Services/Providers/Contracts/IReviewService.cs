﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Services.DTO;

namespace TopBeerShop.Services.Providers.Contracts
{
    public interface IReviewService
    {
        public ReviewDTO GetReview(int id);
        public ICollection<ReviewDTO> GetAllReviews();
        public ReviewDTO CreateReview(ReviewDTO beerDTO);
        public ReviewDTO UpdateReview(int id, ReviewDTO reviewDTO);
        public bool DeleteReview(int id);

        public bool LikeReview(int id);
        public bool DisLikeReview(int id);
        public bool FlagReview(int id);
        public int GetReviewId(string name);


    }
}
