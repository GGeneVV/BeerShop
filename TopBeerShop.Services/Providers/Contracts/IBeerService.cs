﻿using TopBeerShop.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;

namespace TopBeerShop.Services.Contracts
{
    public interface IBeerService 
    {
        public BeerDTO GetBeer(int id);
        public ICollection<BeerDTO> GetAllBeers();
        public BeerDTO CreateBeer(BeerDTO beerDTO);
        public BeerDTO UpdateBeer(BeerDTO beerDTO);
        public bool DeleteBeer(int id);
        public int GetBeerId(string name);
    }
}
