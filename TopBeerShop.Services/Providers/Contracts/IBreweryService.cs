﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Services.Contracts
{
    public interface IBreweryService
    {
        public BreweryDTO GetBrewery(int id);
        public ICollection<BreweryDTO> GetAllBreweries();
        public BreweryDTO CreateBrewery(BreweryDTO breweryDTO);
        public bool DeleteBrewery(int id);
        public BreweryDTO UpdateBrewery(BreweryDTO breweryDTO);

         public bool AddBeerToBrewery(BeerDTO beerDTO, BreweryDTO breweryDTO);
        public int GetId(string name);
    }
}
