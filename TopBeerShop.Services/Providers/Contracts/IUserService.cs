﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Services.Providers.Contracts
{
    public interface IUserService
    {
        UserDTO GetUser(string userName);
        int GetUserId(string name);
        UserDTO GetUser(int Id);
        ICollection<UserDTO> GetAllUsers();
        string GetName(int id);        
        bool DeleteUser(int id);
        UserDTO BanUser(int id);
        UserDTO BanUser(string userName);
    }
}
