﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Services.DTO;

namespace TopBeerShop.Services.Contracts
{
    public interface IWishListService
    {
        public WishListDTO GetWishList(int id);
        public ICollection<WishListDTO> GetAllWishLists();
        public WishListDTO CreateWishList(WishListDTO wishListDTO);  //string wishListName
        public WishListDTO UpdateWishList(int id, WishListDTO wishListDTO);
        public bool DeleteWishList(int id);
        public int GetWishListId(string name);

    }
}
