﻿using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace TopBeerShop.Services.Contracts
{
    public interface ICountryService
    {
        public CountryDTO GetCountry(int id);
       
        public ICollection<CountryDTO> GetAllCountries();
        public CountryDTO CreateCountry(CountryDTO countryDTO);
        public CountryDTO Edit(CountryDTO countryDTO);
        
        public bool DeleteCountry(int id);

        public int GetCountryId(string name);
    }
}
