﻿using System;
using System.Collections.Generic;
using System.Text;
using TopBeerShop.Services.DTOs;

namespace TopBeerShop.Services.Providers.Contracts
{
    public interface IBeerStyleService
    {
        public ICollection<BeerStyleDTO> GetAllBeerStyles(); 
        public BeerStyleDTO CreateBeerStyle(BeerStyleDTO beerStyleDTO);
        public bool AddBeerToBeerStyle(BeerStyleDTO beerStyleDTO, BeerDTO beerDTO);
        public BeerStyleDTO DeleteBeerStyle(int id);
        public int GetId(string name);
        public BeerStyleDTO GetBeerStyle(int id);
        public BeerStyleDTO EditBeerStyle (BeerStyleDTO beerStyleDTO);


    }
}
