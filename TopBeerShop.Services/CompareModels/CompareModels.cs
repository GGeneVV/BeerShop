﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopBeerShop.Models;
using TopBeerShop.Services.DTOs;
using TopBeerShop.Services.MappersDTO;

namespace TopBeerShop.Services.CompareModels
{
    public static class CompareModels
    {
        public static bool CompareBreweryDTO(this BreweryDTO breweryDTO, Brewery breweryToCompare)
        {
            if (
               breweryDTO.BreweryId == breweryToCompare.BreweryId
                && breweryDTO.Name == breweryToCompare.Name
                && breweryDTO.CountryId == breweryToCompare.CountryId
                && breweryDTO.CountryName == breweryToCompare.Country.Name
                && breweryDTO.Address == breweryToCompare.Address
                && breweryDTO.Website == breweryToCompare.Website
                )
            {
                return true;
            }
            return false;
        }

        public static bool CompareCountryDTO(this CountryDTO countryDTO, Country countryToCompare)
        {
            if (countryDTO.Id == countryToCompare.Id && countryDTO.Name == countryToCompare.Name)               
            {
                return true;
            }
            return false;
        }


    }
}

